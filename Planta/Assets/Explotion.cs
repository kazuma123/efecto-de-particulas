﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Explotion : MonoBehaviour
{
    public float minforce;
    public float maxForce;
    public float radius;

    private void Start()
    {
        Explote();
    }
    public void Explote() {
        foreach (Transform t in transform) {
            var rb = t.GetComponent<Rigidbody>();

            if (rb != null) {
                rb.AddExplosionForce(Random.Range (minforce, maxForce), transform.position,radius);
;            }
        }
    }
}
